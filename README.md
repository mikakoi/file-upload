Tämä on harjoitustyö tiedoston turvallisesta lataamisesta palvelimelle. Projektissa on tehty yksinkertainen 
kuvan lataaminen ja galleria ladattujen tiedostojen katseluun. Kaikki koodi ja kommentit on tehty englanniksi ja
tarkempi prosession kuvaus löytyy tästä readme:sta suomeksi.

`$_FILES['file']['error']` Tarkistaa:

* Tiedoston koon
* Onko tiedostoa lähetetty
* Ja muita erilaisia virhetilanteita.
    
`$_FILES['file']['error']` palauttaa yksinkertaisen kokonaisluvun jota vertaamme php constantteihin.
Koodissa vertaamme sitä mm. 

* UPLOAD_ERR_NO_FILE
* UPLOAD_ERR_INI_SIZE
* UPLOAD_ERR_FORM_SIZE

Jos on tarve antaa
käyttäjälle enemmän tietoa virheestä kaikki constantit liittyen virheeseen löydät täältä: 
http://php.net/manual/en/features.file-upload.errors.php

Jos jostain syystä php.ini:ssä on määritelty "upload_max_filesize" epäturvallisen korkeaksi, teemme itse tarkistuksen.
Tarkistaminen kannattaa tehdä `$_FILES['file']['size']`:lla vertamalla sitä vastaanotetun tiedoston kanssa.

Koska emme voi antaa käyttäjän ladata ihan mitä tahansa tiedostoja, esim. hyökkääjä voisi ladata php tiedoston
ja suorittaa kyseisen tiedoston jos emme tarkista tiedoston tyyppiä. Tarkistamme tiedston tyypin seuraavasti.
Hyäkkääjä voi helposti vaihtaa $_FILES['file']['type'] tiedon vaihtamalla header datan joten emme voi luottaa siihen.
Tiedoston tyyppiä pitää tarkistaa finfo oliolla. 
	`$finfo->file($_FILES['file']['tmp_name'])` antaa infoa tiedoston tyypistä.

Tiedostolle emme voi antaa nimeä joka on määritelty `$_FILES['file']['name']`, koska siinä on mahdollisesti käytetty
directory seperatoreita, josta syystä tiedostoa ei siirretäisikään oikeaan kansioon.
Tiedostolle annetaan uniikki nimi käyttäen sha1_file funktiota joka palauttaa ladatun tiedoston binääristä sha1 hashin.
Tiedoston nimen perään pistämme tiedoston päätteen jonka olemme todenneet turvalliseksi aikaisemmassa vaiheessa

Jos kaikki tarkastukset tähän mennessä on suoritettu ilman virheitä, on tiedosto turvallisesti ladattu palvelimelle.