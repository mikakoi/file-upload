<html>
<head>
    <title>File upload example</title>
    <link rel="stylesheet" type="text/css" href="js/lightbox2/css/lightbox.min.css">
    <link rel="stylesheet" type="text/css" href="style.css">
    <script type="text/javascript" src="js/lightbox2/js/lightbox-plus-jquery.min.js"></script>
</head>
<body>
    <h2>File Upload</h2>
    <form action="upload.php" method="POST" enctype="multipart/form-data">
        <table>
            <tr><td>Select image to upload:</td></tr>
            <tr><td><input type="file" name="file" id="file"></td></tr>
            <tr><td><input type="submit" value="Upload Image" name="submit"></td></tr>
        </table>
    </form>
    <h2>Gallery</h2>
    <?php $dir = 'uploads/'; ?>
    <?php $images = scandir($dir); ?>
    <?php foreach ($images as $image): ?>
        <?php if(is_file($dir . $image)): ?>
            <a href="<?= $dir . $image; ?>" class="img-link" data-lightbox="img"><img src="<?= $dir . $image; ?>"></a>
        <?php endif ?>
    <?php endforeach ?>
</body>
</html>