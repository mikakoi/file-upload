<?php
$maxFileSize = 100000000; // 100000000 Bytes = 100 Megabytes
$allowedTypes = [
    'jpg' => 'image/jpeg',
    'png' => 'image/png',
    'gif' => 'image/gif'
];

try {
    // Undefined | Multiple Files | $_FILES Corruption Attack
    // If this request falls under any of them, treat it invalid.
    if (!isset($_FILES['file']['error']) || is_array($_FILES['file']['error'])) {
        throw new RuntimeException('Invalid parameters.');
    }

    // Check $_FILES['file']['error'] value.
    switch ($_FILES['file']['error']) {
        case UPLOAD_ERR_OK:
            break;
        case UPLOAD_ERR_NO_FILE:
            throw new RuntimeException('No file sent.');
        case UPLOAD_ERR_INI_SIZE:
        case UPLOAD_ERR_FORM_SIZE:
            throw new RuntimeException('File is too big.');
        default:
            throw new RuntimeException('Unknown error.');
    }

    // In case php.ini has insecure max_file_size value, check the size here
    if ($_FILES['file']['size'] > $maxFileSize) {
        throw new RuntimeException('File is too big.');
    }
    
    // Do not trust $_FILES['file']['type'] value!
    // Check MIME Type by yourself.
    $finfo = new finfo(FILEINFO_MIME_TYPE);
    if (false === $ext = array_search(
        $finfo->file($_FILES['file']['tmp_name']),
        $allowedTypes,
        true
    )) {
        throw new RuntimeException('Invalid file format.');
    }
    
    // Create upload folder if it doesn't exist
    if (!file_exists('./uploads/')) {
        mkdir('./uploads/', 0755, true);
    }

    // Give the file an unique name
    // Uploader has direct control over $_FILES['file']['name'], so don't use it without validation
    // Here we obtain an unique with sha1 hash of the file
    if (!move_uploaded_file($_FILES['file']['tmp_name'],
        sprintf('./uploads/%s.%s',
            sha1_file($_FILES['file']['tmp_name']),
            $ext
        )
    )) {
        throw new RuntimeException('Failed to move the uploaded file.');
    }

    // Upload was successful
    echo 'File is uploaded successfully.';

} catch (RuntimeException $e) {
    echo $e->getMessage();
}
?>
<br>
<a href="./">Back</a>

